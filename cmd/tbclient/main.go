// Tbclient connects to the tbsvr process to allow control of the inVADER
// Timing Board FPGA
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/tbctl/pkg/tb"
	"github.com/BurntSushi/toml"
	"github.com/briandowns/spinner"
)

const Usage = `Usage: tbclient [options] paramfile

Connect to the Timing Board server, load new parameters into the FPGA
and optionally enable the output.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	enableFpga = flag.Bool("enable", false,
		"If true, enable FPGA output")
	svrAddr  string = "/run/tb.sock"
	readRegs bool
)

var FpgaEnable tb.Message = tb.Message{Cmd: "enable"}
var FpgaDisable tb.Message = tb.Message{Cmd: "disable"}
var FpgaLoad tb.Message = tb.Message{Cmd: "load"}
var FpgaRead tb.Message = tb.Message{Cmd: "read"}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&svrAddr, "sock", svrAddr,
		"TB server socket")
	flag.BoolVar(&readRegs, "read", readRegs, "read the current register values")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func sendMessages(sock string, msgs ...tb.Message) error {
	conn, err := net.Dial("unix", sock)
	if err != nil {
		return fmt.Errorf("Server connect failed: %v", err)
	}
	defer conn.Close()

	enc := json.NewEncoder(conn)
	for _, msg := range msgs {
		err := enc.Encode(msg)
		if err != nil {
			return err
		}
	}
	return nil
}

func readValues(sock string, timeo time.Duration) (tb.Timing, error) {
	var tm tb.Timing

	conn, err := net.Dial("unix", sock)
	if err != nil {
		return tm, fmt.Errorf("Server connect failed: %v", err)
	}
	defer conn.Close()

	err = json.NewEncoder(conn).Encode(FpgaRead)
	if err != nil {
		return tm, err
	}
	conn.SetReadDeadline(time.Now().Add(timeo))
	err = json.NewDecoder(conn).Decode(&tm)
	return tm, err
}

type settings struct {
	Fclk int64     `toml:"Fclk"`
	T    tb.Timing `toml:"timing"`
}

func main() {
	args := parseCmdLine()

	if len(args) < 1 && !readRegs {
		flag.Usage()
		os.Exit(1)
	}

	if readRegs {
		tm, err := readValues(svrAddr, time.Second*5)
		if err != nil {
			log.Println(err)
		}
		json.NewEncoder(os.Stdout).Encode(tm)
		return
	}

	contents, err := ioutil.ReadFile(args[0])
	if err != nil {
		log.Fatalf("Cannot read parameter file; %v", err)
	}

	var params settings
	if _, err := toml.Decode(string(contents), &params); err != nil {
		log.Fatalf("Cannot parse parameters: %v", err)
	}

	err = params.T.Validate(params.Fclk)
	if err != nil {
		log.Fatalf("Parameter file error; %v", err)
	}

	FpgaLoad.Data, _ = json.Marshal(params.T)

	err = sendMessages(svrAddr, FpgaDisable, FpgaLoad)
	if err != nil {
		log.Fatal(err)
	}

	if *enableFpga {
		err = sendMessages(svrAddr, FpgaEnable)
		if err != nil {
			log.Fatal(err)
		}

		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
		defer signal.Stop(sigs)

		s := spinner.New(spinner.CharSets[9], 100*time.Millisecond)
		s.Prefix = "Output enabled, type ctrl-c to quit ... "
		s.FinalMSG = "\n"
		s.Start()
		<-sigs
		s.Stop()

		err = sendMessages(svrAddr, FpgaDisable)
		if err != nil {
			log.Fatal(err)
		}
	}
}
