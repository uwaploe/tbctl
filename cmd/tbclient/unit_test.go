package main

import (
	"encoding/json"
	"testing"
	"time"

	"bitbucket.org/uwaploe/tbctl/pkg/tb"
	"github.com/BurntSushi/toml"
)

var CFG = `[timing]
# Laser diode trigger period
Dfreq = "200ms"
# Laser diode trigger pulse width
EDon = "1ms"
# Qswitch delay
QSdelay = "200ms"
# Qswitch pulse width
QSon = "1ms"
# Qswitch + CCD trigger delay
QSETdelay = "20ms"
# CCD trigger pulse width
ETon = "1ms"
# Qswitch + CCD trigger + CCD gate delay
QSETEGdelay = "30ms"
# CCD gate pulse width
EGon = "1ms"
`

func TestLoadMsg(t *testing.T) {
	var params settings
	if _, err := toml.Decode(CFG, &params); err != nil {
		t.Fatalf("Cannot parse parameters: %v", err)
	}

	expected := tb.Duration(time.Millisecond)
	if params.T.QSon != expected {
		t.Errorf("Bad parameter; expected %v, got %v", expected,
			params.T.QSon)
	}

	FpgaLoad.Data, _ = json.Marshal(params.T)
	b, err := json.Marshal(FpgaLoad)
	if err != nil {
		t.Error(err)
	}

	msg := tb.Message{}
	err = json.Unmarshal(b, &msg)
	if err != nil {
		t.Fatal(err)
	}
	b, err = msg.ToSpi()
	if err != nil {
		t.Fatal(err)
	}
}
