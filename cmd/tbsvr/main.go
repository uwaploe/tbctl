// Tbsvr is a Unix Domain server which controls the inVADER Timing Board.
package main

import (
	"encoding/json"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	spi "bitbucket.org/uwaploe/bengalspi"
	"bitbucket.org/uwaploe/tbctl/pkg/tb"
	"github.com/coreos/go-systemd/v22/activation"
)

var Version = "dev"
var BuildDate = "unknown"

const svcAddr = "/var/tmp/tb.sock"

// SPI bus configuration for Timing Board FPGA
var spiSetup = spi.Config{
	Polarity: spi.Low,
	Phase:    spi.RisingEdge,
	Size:     spi.Spi24bit,
	SlaveSel: 1,
	Clock:    spi.Freq2Mhz,
	Order:    spi.MsbFirst,
}

func tbService(conn net.Conn, timeo time.Duration, bus *spi.Bus) {
	defer conn.Close()
	dec := json.NewDecoder(conn)
	for {
		var (
			m   tb.Message
			err error
		)
		conn.SetReadDeadline(time.Now().Add(timeo))
		if err = dec.Decode(&m); err == io.EOF {
			break
		} else if err != nil {
			log.Println(err)
			return
		}

		log.Printf("Command: %s", m)
		msg := spi.Message{}
		msg.Out, err = m.ToSpi()
		if err != nil {
			log.Println(err)
			return
		}

		_, err = bus.DoXfer(&msg)
		if err != nil {
			log.Printf("SPI transfer failed: %v", err)
			return
		}

		// Return the Timing parameter settings
		if m.Cmd == "read" {
			log.Printf("SPI input: %q", msg.In)
			tm := tb.Timing{}
			if err := tm.UnmarshalSPI(tb.FpgaClock, msg.In); err != nil {
				log.Println(err)
			}
			json.NewEncoder(conn).Encode(tm)
		}
	}
}

func main() {
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		log.SetFlags(0)
	}

	bus := spi.NewBus(spi.IsaOffset, false)
	if _, ok := os.LookupEnv("SPI_DEBUG"); ok {
		bus.SetDebug(true)
	}

	if _, ok := os.LookupEnv("SPI_LOOPBACK"); ok {
		bus.SetLoopback(true)
		defer bus.SetLoopback(false)
	} else {
		bus.SetLoopback(false)
	}

	err := bus.Setup(spiSetup)
	if err != nil {
		log.Fatalf("Cannot configure SPI interface: %v", err)
	}
	bus.DoXfer(&spi.Message{Out: []byte(tb.FpgaDisable)})

	listeners, err := activation.Listeners()
	if err != nil {
		log.Fatalf("Systemd activation error: %v", err)
	}

	var listener net.Listener
	switch n := len(listeners); n {
	case 0:
		// Not activated by a Systemd socket ...
		if err := os.RemoveAll(svcAddr); err != nil {
			log.Fatal(err)
		}
		addr, _ := net.ResolveUnixAddr("unix", svcAddr)
		listener, err = net.ListenUnix("unix", addr)
		if err != nil {
			log.Fatal(err)
		}
	case 1:
		listener = listeners[0]
	default:
		log.Fatalf("Unexpected number of socket activation fds: %d", n)
	}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Ensure the FPGA output is disabled when we exit.
	defer bus.DoXfer(&spi.Message{Out: []byte(tb.FpgaDisable)})

	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			listener.Close()
		}
	}()

	log.Printf("Timing Board controller (%s), Waiting for connections", Version)
	for {
		c, err := listener.Accept()
		if err != nil {
			log.Println("Exiting ...")
			break
		}

		tbService(c, time.Second*3, bus)
	}
}
