// Package tb implements a data structure to hold the inVADER Timing Board
// trigger parameters.
package tb

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"
)

// See https://stackoverflow.com/a/54571600
type Duration time.Duration

func (d Duration) MarshalJSON() ([]byte, error) {
	return json.Marshal(time.Duration(d).String())
}

func (d *Duration) UnmarshalJSON(b []byte) error {
	var v interface{}
	if err := json.Unmarshal(b, &v); err != nil {
		return err
	}
	switch value := v.(type) {
	case float64:
		*d = Duration(time.Duration(value))
		return nil
	case string:
		tmp, err := time.ParseDuration(value)
		if err != nil {
			return err
		}
		*d = Duration(tmp)
		return nil
	default:
		return errors.New("invalid duration")
	}
}

func (d Duration) MarshalText() ([]byte, error) {
	return []byte(time.Duration(d).String()), nil
}

func (d *Duration) UnmarshalText(text []byte) error {
	val, err := time.ParseDuration(string(text))
	if err != nil {
		return err
	}
	*d = Duration(val)
	return nil
}

// Timing represents the parameters from the inVADER Timing Board FPGA. The
// "addr" annotations give the FPGA register address(es) for each parameter.
type Timing struct {
	// Laser diode trigger period
	Dfreq Duration `addr:"0x10,0x11"`
	// Laser diode trigger pulse width
	EDon Duration `addr:"0x12"`
	// Qswitch delay
	QSdelay Duration `addr:"0x13,0x14"`
	// Qswitch pulse width
	QSon Duration `addr:"0x15"`
	// Qswitch + CCD trigger delay
	QSETdelay Duration `addr:"0x16,0x17"`
	// CCD trigger pulse width
	ETon Duration `addr:"0x18"`
	// Qswitch + CCD trigger + CCD gate delay
	QSETEGdelay Duration `addr:"0x19,0x1a"`
	// CCD gate pulse width
	EGon Duration `addr:"0x1b"`
}

// Validate checks the Timing struct values to ensure that none will overflow
// the FPGA registers
func (tm Timing) Validate(clkFreq int64) error {
	t := reflect.TypeOf(tm)
	v := reflect.ValueOf(tm)
	badvals := make([]string, 0, t.NumField())

	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		// Convert value to clock ticks
		ns := int64(time.Duration(v.Field(i).Interface().(Duration)) / time.Nanosecond)
		ticks := (ns * clkFreq) / 1e9
		if tagval, ok := f.Tag.Lookup("addr"); ok {
			addrs := strings.Split(tagval, ",")
			switch len(addrs) {
			case 1:
				if ticks > 0xffff {
					badvals = append(badvals, f.Name)
				}
			case 2:
				if ticks > 0xffffffff {
					badvals = append(badvals, f.Name)
				}
			}
		}
	}

	if len(badvals) > 0 {
		return fmt.Errorf("Timing values out of range: %q", badvals)
	}

	return nil
}

const MessageLen = 36

// SPI message frame is sent MSB first
func writeTransaction(addr uint8, val uint16) [3]byte {
	var b [3]byte

	b[2] = addr
	b[1] = uint8(val >> 8)
	b[0] = uint8(val & 0xff)
	return b
}

func readTransaction(addr uint8) [3]byte {
	var b [3]byte

	b[2] = addr | 0x80
	b[1] = 0
	b[0] = 0
	return b
}

func parseFrame(b []byte) (uint8, uint16) {
	val := uint16(b[1])<<8 | uint16(b[0])
	return b[2], val
}

// ReadAllMsg constructs a message for the FPGA which will read back all
// of the register settings.
func (tm Timing) ReadAllMsg() []byte {
	var (
		x uint64
		j int
	)

	t := reflect.TypeOf(tm)

	buf := make([]byte, 0, MessageLen)
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		tagval := f.Tag.Get("addr")
		addrs := strings.Split(tagval, ",")
		j = 0
		if len(addrs) == 2 {
			x, _ = strconv.ParseUint(addrs[j], 0, 8)
			b := readTransaction(uint8(x))
			buf = append(buf, b[:]...)
			j++
		}
		x, _ = strconv.ParseUint(addrs[j], 0, 8)
		b := readTransaction(uint8(x))
		buf = append(buf, b[:]...)
	}

	return buf
}

// MarshalSPI creates a slice of bytes to send to the FPGA via the SPI
// bus. Clkfreq is the clock frequency in Hz and is used to convert the
// durations to clock ticks.
func (tm Timing) MarshalSPI(clkFreq int64) ([]byte, error) {
	var (
		x   uint64
		err error
		j   int
	)

	t := reflect.TypeOf(tm)
	v := reflect.ValueOf(tm)

	buf := make([]byte, 0, MessageLen)
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		// Convert value to clock ticks
		ns := int64(time.Duration(v.Field(i).Interface().(Duration)) / time.Nanosecond)
		ticks := (ns * clkFreq) / 1e9
		if tagval, ok := f.Tag.Lookup("addr"); ok {
			addrs := strings.Split(tagval, ",")
			j = 0
			if len(addrs) == 2 {
				// Upper word of a 4-byte value
				x, err = strconv.ParseUint(addrs[j], 0, 8)
				if err != nil {
					return buf, err
				}
				b := writeTransaction(uint8(x), uint16(ticks>>16))
				buf = append(buf, b[:]...)
				j++
			}

			x, err = strconv.ParseUint(addrs[j], 0, 8)
			b := writeTransaction(uint8(x), uint16(ticks))
			buf = append(buf, b[:]...)
		}
	}

	return buf, nil
}

var ErrSize = errors.New("Bad message size")

// UnmarshalSPI extracts the contents of a Timing instance from a slice of
// bytes received from the FPGA via the SPI bus.
func (tm *Timing) UnmarshalSPI(clkFreq int64, b []byte) error {
	if len(b) < MessageLen {
		return ErrSize
	}

	v := reflect.ValueOf(tm)
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	t := v.Type()

	bstart := 0
	for i := 0; i < v.NumField(); i++ {
		f := v.Field(i)
		if !f.CanSet() {
			return fmt.Errorf("Cannot set value: %q", f)
		}

		ticks := int64(0)
		tagval := t.Field(i).Tag.Get("addr")
		addrs := strings.Split(tagval, ",")

		if len(addrs) == 2 {
			_, regval := parseFrame(b[bstart : bstart+3])
			bstart += 3
			ticks = int64(regval) << 16
		}

		_, regval := parseFrame(b[bstart : bstart+3])
		bstart += 3
		ticks += int64(regval)
		val := Duration(time.Duration(ticks*1e9/clkFreq) * time.Nanosecond)
		f.Set(reflect.ValueOf(val))

	}

	return nil
}
