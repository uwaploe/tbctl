package tb

import (
	"encoding/json"
	"fmt"
)

// FPGA clock frequency
const FpgaClock = 200000000

// SPI message to enable FPGA output
const FpgaEnable = "\x01\x00\x00"

// SPI message to disable FPGA output
const FpgaDisable = "\x00\x00\x00"

// API messages
type Message struct {
	Cmd  string          `json:"cmd"`
	Data json.RawMessage `json:"data,omitempty"`
}

// String implements the Stringer interface
func (m Message) String() string {
	return fmt.Sprintf("%s [%s]", m.Cmd, string(m.Data))
}

// Scan unmarshals the Data element into the value pointed to by v.
func (m Message) Scan(v interface{}) error {
	return json.Unmarshal(m.Data, v)
}

// ToSpi converts an API message to an SPI bus message
func (m Message) ToSpi() ([]byte, error) {
	var (
		err error
		b   []byte
	)

	switch m.Cmd {
	case "enable":
		b = []byte(FpgaEnable)
	case "disable":
		b = []byte(FpgaDisable)
	case "load":
		timing := Timing{}
		err = m.Scan(&timing)
		if err != nil {
			err = fmt.Errorf("Invalid message data: %q", m.Data)
		} else {
			b, err = timing.MarshalSPI(FpgaClock)
		}
	case "read":
		var tm Timing
		b = tm.ReadAllMsg()
	default:
		err = fmt.Errorf("Unknown command: %q", m.Cmd)
	}

	return b, err
}
