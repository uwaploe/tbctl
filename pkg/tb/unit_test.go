package tb

import (
	"reflect"
	"testing"
	"time"
)

func TestMarshal(t *testing.T) {
	table := []struct {
		in  Timing
		clk int64
		out []byte
	}{
		{
			in: Timing{
				Dfreq:       Duration(50 * time.Millisecond),
				EDon:        Duration(30 * time.Microsecond),
				QSdelay:     Duration(200 * time.Microsecond),
				QSon:        Duration(30 * time.Microsecond),
				QSETdelay:   Duration(200*time.Microsecond - 100*time.Nanosecond),
				ETon:        Duration(30 * time.Microsecond),
				QSETEGdelay: Duration(200*time.Microsecond - 100*time.Nanosecond),
				EGon:        Duration(300 * time.Microsecond),
			},
			clk: 200000000,
			out: []byte{
				0x98, 0x00, 0x10,
				0x80, 0x96, 0x11,
				0x70, 0x17, 0x12,
				0x00, 0x00, 0x13,
				0x40, 0x9c, 0x14,
				0x70, 0x17, 0x15,
				0x00, 0x00, 0x16,
				0x2c, 0x9c, 0x17,
				0x70, 0x17, 0x18,
				0x00, 0x00, 0x19,
				0x2c, 0x9c, 0x1a,
				0x60, 0xea, 0x1b},
		},
	}

	for _, e := range table {
		b, err := e.in.MarshalSPI(e.clk)
		if err != nil {
			t.Fatal(err)
		}
		if string(b) != string(e.out) {
			t.Errorf("Bad SPI message; expected %q, got %q", e.out, b)
		}

		tm := Timing{}
		err = tm.UnmarshalSPI(e.clk, b)
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(tm, e.in) {
			t.Errorf("Unmarshal failed; expected %#v, got %#v", e.in, tm)
		}
	}
}

func TestValidate(t *testing.T) {
	good := Timing{
		Dfreq:       Duration(50 * time.Millisecond),
		EDon:        Duration(30 * time.Microsecond),
		QSdelay:     Duration(200 * time.Microsecond),
		QSon:        Duration(30 * time.Microsecond),
		QSETdelay:   Duration(200*time.Microsecond - 100*time.Nanosecond),
		ETon:        Duration(30 * time.Microsecond),
		QSETEGdelay: Duration(200*time.Microsecond - 100*time.Nanosecond),
		EGon:        Duration(50 * time.Nanosecond),
	}

	bad := Timing{
		Dfreq:       Duration(50 * time.Millisecond),
		EDon:        Duration(500 * time.Microsecond),
		QSdelay:     Duration(200 * time.Microsecond),
		QSon:        Duration(30 * time.Microsecond),
		QSETdelay:   Duration(200*time.Microsecond - 100*time.Nanosecond),
		ETon:        Duration(30 * time.Microsecond),
		QSETEGdelay: Duration(200*time.Microsecond - 100*time.Nanosecond),
		EGon:        Duration(300 * time.Millisecond),
	}

	clk := int64(200000000)
	err := good.Validate(clk)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	err = bad.Validate(clk)
	if err == nil {
		t.Error("Bad value not detected")
	} else {
		t.Logf("Detected bad value; %v", err)
	}
}

func TestReadAllMsg(t *testing.T) {
	expected := []byte{
		0x00, 0x00, 0x10 | 0x80,
		0x00, 0x00, 0x11 | 0x80,
		0x00, 0x00, 0x12 | 0x80,
		0x00, 0x00, 0x13 | 0x80,
		0x00, 0x00, 0x14 | 0x80,
		0x00, 0x00, 0x15 | 0x80,
		0x00, 0x00, 0x16 | 0x80,
		0x00, 0x00, 0x17 | 0x80,
		0x00, 0x00, 0x18 | 0x80,
		0x00, 0x00, 0x19 | 0x80,
		0x00, 0x00, 0x1a | 0x80,
		0x00, 0x00, 0x1b | 0x80}

	var tm Timing
	out := tm.ReadAllMsg()
	if string(out) != string(expected) {
		t.Errorf("Failed; expected %q, got %q", expected, out)
	}
}
