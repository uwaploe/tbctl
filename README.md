# inVADER Timing Board Control

This repository provides a Unix-domain server to control the inVADER Timing Board FPGA along with a command-line client.

## Server

The server runs as a [Systemd](https://www.freedesktop.org/wiki/Software/systemd/) service listening for connections on `/run/tb.sock` and accepting  the following JSON formatted commands. The server needs to run with root privileges to access the SPI interface.

### Enable

Enables the FPGA output.

``` json
{"cmd":"disable"}
```

### Disable

Disables the FPGA output.

``` json
{"cmd":"disable"}
```

### Load

Loads new FPGA timing parameters. All of the time values consist of a decimal number with a optional fraction and a unit suffix. Valid suffixes are ns (nanoseconds), us or µs (microseconds), ms (milliseconds), and s (seconds).

``` json
{
  "cmd": "load",
  "data": {
    "Dfreq": "50ms",
    "EDon": "30µs",
    "QSdelay": "200µs",
    "QSon": "30µs",
    "QSETdelay": "199.9µs",
    "ETon": "30µs",
    "QSETEGdelay": "199.9µs",
    "EGon": "50ns"
  }
}
```

## Command-line Client

### Usage

``` shellsession
sysop@invader-sic-1:~$ tbclient --help
Usage: tbclient [options] paramfile

Connect to the Timing Board server, load new parameters into the FPGA
and optionally enable the output.
  -debug
        If true enable debugging output
  -enable
        If true, enable FPGA output
  -sock string
        TB server socket (default "/run/tb.sock")
  -version
        Show program version information and exit
```

### Parameter File

The parameters are stored in a [TOML](https://github.com/toml-lang/toml) format file which is very similar to the Windows "INI" format.

``` toml
# inVADER Timing Board parameters
#
[timing]
# Laser diode trigger period
Dfreq = "50ms"
# Laser diode trigger pulse width
EDon = "30us"
# Qswitch delay
QSdelay = "200us"
# Qswitch pulse width
QSon = "30us"
# Qswitch + CCD trigger delay
QSETdelay = "199.9us"
# CCD trigger pulse width
ETon = "30us"
# Qswitch + CCD trigger + CCD gate delay
QSETEGdelay = "199.9us"
# CCD gate pulse width
EGon = "50ns"
```
