module bitbucket.org/uwaploe/tbctl

go 1.13

require (
	bitbucket.org/uwaploe/bengalspi v0.3.0
	github.com/BurntSushi/toml v0.3.1
	github.com/briandowns/spinner v1.9.0
	github.com/coreos/go-systemd/v22 v22.0.0
)
